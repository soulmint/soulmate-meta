Source: soulmate-meta
Section: metapackages
Priority: optional
Maintainer: Luis Antonio Garcia Gisbert <luisgg@gmail.com>
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 7.0.8), germinate (>= 2.25-0aurex1), rsync, bzr, debootstrap

Package: soulmate-base-files
Architecture: all
Conflicts: soulmate-transmute (<< 18.08.06), sm-zfs (<< 1:9.5+soulmate18.09.20)
Description: base files for soulmate
 Important miscellaneous files for soulmate

Package: soulmate-translive-base-files
Architecture: all
Description: base files for soulmate translive
 Soulmate base files data for soulmate translive

Package: sm-minimal-net
Architecture: all
Depends:  bridge-utils, vlan, ifenslave, net-tools, iproute2
Description: Minimal soulmate network tools
 Minimal set of packages related to networking required to run soulmate

Package: sm-minimal-fs
Architecture: all
Depends:  lvm2, dmsetup, mdadm, reiserfsprogs, xfsprogs, ntfs-3g
Description: Minimal soulmate filesystem tools
 Minimal set of packages related to filesystems required to run soulmate

Package: sm-minimal-misc
Architecture: all
Depends: soulmate-transmute, sudo
Description: Minimal soulmate miscellaneous tools
 Minimal set of miscellaneous packages required to run soulmate

Package: soulmate-minimal
Architecture: any
Depends: soulmate-base-files, sm-minimal-net (= ${binary:Version}), sm-minimal-fs (= ${binary:Version}), sm-minimal-misc (= ${binary:Version})
Description: Soulmate minimal system
 This (meta)package depends on all of the packages for a
 minimal soulmate system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: sm-standard-net
Architecture: all
Depends:  sm-minimal-net (= ${binary:Version}), tcpdump, dnsutils, ftp, iptables, rsync, wget, axel, netcat, nfs-common, smbclient, cifs-utils, glusterfs-client, sshfs, traceroute, ntpdate, ldap-utils, screen
Recommends: openssh-client, curl
Description: Standard soulmate network tools
 Standard set of packages related to networking required to run soulmate

Package: sm-standard-fs
Architecture: all
Depends: sm-minimal-fs (= ${binary:Version}), dosfstools, parted, gdisk, mc, cdrdao
Description: Standard soulmate filesystem tools
 Standard set of packages related to filesystem required to run soulmate

Package: sm-standard-misc
Architecture: all
Depends: sm-minimal-misc (= ${binary:Version}), zip, unzip, unrar, arj, lhasa, cabextract, rpm, squashfs-tools, debconf-utils, ophcrack-cli, tree, vim, bash-completion
Recommends: rar, unace, p7zip-full, p7zip-rar, sharutils, uudeview, mpack, cabextract
Description: Standard soulmate miscellaneous tools
 Standard set of miscellaneous packages required to run soulmate

Package: soulmate-standard
Architecture: any
Depends: sm-standard-net (= ${binary:Version}), sm-standard-fs (= ${binary:Version}), sm-standard-misc (= ${binary:Version})
Description: Soulmate standard system
 This (meta)package depends on all of the packages in the standard
 soulmate system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: sm-meta-debian-core
Architecture: all
Depends: soulmate-keyring, mint-info-cinnamon, mint-artwork, debian-system-adjustments, mint-translations, mintbackup, mintinstall,  mintsystem, mintupdate, mint-mirrors, mintsources, p7zip, command-not-found, inxi
Provides: mint-meta-core
Replaces: mint-meta-core, mint-meta-common, mint-meta-debian, mint-meta-debian-core
Conflicts: mint-meta-core, mint-meta-common, mint-meta-debian, mint-meta-debian-core
Description: Core soulmate packages
 Core soulmate packages

Package: sm-software-properties
Architecture: all
Depends: software-properties-gtk, python-apt
Conflicts: mintsources
Description: software-properties-gtk soulmate package
 Soulmate compatible software-properties-gtk

Package: sm-meta-debian-cinnamon
Architecture: all
Depends: sm-meta-debian-core (= ${binary:Version}), mint-meta-cinnamon, samba, samba-common-bin, smbclient, ttf-dejavu, ttf-dejavu-extra, ttf-wqy-microhei, gdebi, pidgin, thunderbird, fonts-droid-fallback, fonts-liberation, fonts-liberation2, mintstick, hexchat, xdg-user-dirs-gtk, cinnamon-control-center, cinnamon, sm-icons (= ${binary:Version}), cinnamon-session, cinnamon-settings-daemon, soulmate-artwork, gnome-terminal, mintmenu, folder-color-switcher, mint-themes, mint-backgrounds-tara, soulmate-themes, lightdm, slick-greeter, lightdm-settings
Description: Set of packages required to run cinnamon
 Set of packages required to run the cinnamon Desktop Environment.

Package: debian-cinnamon-default-settings
Architecture: all
Depends: sm-meta-debian-cinnamon (= ${binary:Version})
Description: Override default Debian settings for cinnamon
 This package replaces Debian settings for cinnamon

Package: sm-cinnamon2d-set-default
Architecture: all
Depends: cinnamon-common
Description: Set default session to cinnamon2d
 Disable graphic card acceleration for cinnamon

Package: desktop-base
Architecture: all
Description: common files for the soulmate Desktop
 This package replaces Debian desktop-base

Package: sm-icons
Architecture: all
Recommends: adwaita-icon-theme,gnome-icon-theme, gnome-icon-theme-symbolic, gtk-update-icon-cache, hicolor-icon-theme, icoutils, libtext-iconv-perl, mint-x-icons, mint-y-icons, rhythmbox-plugin-tray-icon
Description: Soulmate font packages
 Icons packages required to run soulmate desktop

Package: sm-fonts
Architecture: all
Recommends: fonts-noto-cjk, fonts-noto-hinted, fonts-nanum, fonts-opensymbol, fonts-liberation, fonts-kacst-one, fonts-khmeros-core, fonts-liberation, fonts-nanum, fonts-opensymbol, fonts-takao-pgothic, fonts-thai-tlwg, ttf-indic-fonts-core, ttf-punjabi-fonts, ttf-ubuntu-font-family, ttf-wqy-microhei, ttf-dejavu-core, ttf-freefont, fonts-beng, fonts-deva, fonts-gujr, fonts-guru, fonts-indic, fonts-knda, fonts-mlym, fonts-orya, fonts-taml, fonts-telu, ttf-mscorefonts-installer
Description: Soulmate font packages
 Font packages required to run soulmate desktop

Package: sm-extra-fonts
Architecture: all
Recommends: fonts-cantarell, fonts-gubbi, fonts-indic, fonts-knda, fonts-lklug-sinhala, fonts-lohit-orya, fonts-navilu, fonts-orya, fonts-orya-extra, fonts-pagul, fonts-sil-padauk, fonts-tibetan-machine
Description: Soulmate extra font packages
 Extra font packages for soulmate desktop

Package: sm-office
Architecture: all
Recommends: libreoffice-gtk, libreoffice-writer, libreoffice-calc, libreoffice, libreoffice-gnome, libreoffice-style-tango, libreoffice-pdfimport, libreoffice-math, libreoffice-ogltrans, libreoffice-l10n-es, libreoffice-help-es, libreoffice-l10n-ca, libreoffice-help-ca, libreoffice-l10n-en-gb, libreoffice-help-en-gb, myspell-es, myspell-en-gb, myspell-ca, libreoffice-settings
Description: Soulmate office packages
 Office packages required to tun soulmate desktop

Package: sm-multimedia
Architecture: all
Recommends: libdvd-pkg, vlc, gimp, gimp-data, gimp-data-extras, inkscape, sozi, cheese, media-player-info, gstreamer1.0-plugins-ugly, gstreamer1.0-pulseaudio
Description: Soulmate multimedia packages
 Multimedia packages required to tun soulmate desktop

Package: sm-inet
Architecture: any
Depends: soulmate-base-files (= ${binary:Version})
Recommends: mint-flashplugin, chromium, chromium-l10n, flashplayer-chromium, chromium-shell, chromium-driver, chromium-widevine, google-chrome-stable, chrome-keyring-fix, firefox-esr, firefox-esr-l10n-es, firefox-esr-l10n-ca, firefox-esr-l10n-en-gb, filezilla, bluegriffon
Description: Soulmate inet packages
 Inet related packages required to tun soulmate desktop

Package: sm-nodejs
Architecture: any
Depends: lsb-release, apt-transport-https, curl, gcc, g++, make, build-essential, nodejs, code [amd64] 
Recommends: yarn
Description: Soulmate nodejs packages
 nodejs packages for soulmate development metapackage

Package: sm-java
Architecture: any
Depends: oracle-java8-installer, oracle-java8-set-default [amd64], oracle-java8-set-default [i386]
Description: Soulmate java packages
 Java packages required to tun soulmate desktop

Package: sm-java-supported
Architecture: all
Recommends: oracle-java7-installer, oracle-java8-installer, oracle-java9-set-default, oracle-java9-unlimited-jce-policy, oracle-java8-unlimited-jce-policy, oracle-java10-installer, oracle-java10-set-default
Description: Soulmate supported java packages
 Additional Java packages for soulmate desktop

Package: sm-misc
Architecture: all
Recommends: gnome-calculator, gsmartcontrol
Description: Soulmate desktop miscellaneous tools
 Miscellaneous packages required to run soulmate desktop

Package: sm-ffmpeg
Architecture: all
Recommends: libavcodec-dev, ffmpeg, ffmpeg-doc, libavcodec57, libavdevice-dev, libavdevice57, libavfilter-dev, libavfilter6, libavformat-dev, libavformat57, libavresample-dev, libavresample3, libavutil-dev, libavutil55, libpostproc-dev, libpostproc54, libswresample-dev, libswresample2, libswscale-dev, libswscale4
Description: ffmpeg related supported packages
 Additional ffmpeg packages for soulmate

Package: sm-mate
Architecture: all
Recommends: mate-desktop, mate-desktop-environment
Description: mate desktop support for soulmate
 This package install necessary files to start a Mate Desktop session in soulmate

Package: sm-supported
Architecture: any
Recommends: sm-java-supported, sm-ffmpeg, libvirt-bin, libvirt0, python-libvirt, virt-manager, virt-viewer, virtinst, qemu-kvm, quemu, virtualbox-4.3, virtualbox-5.0, germinate, dropbox, ldap-utils, opera, samba, samba-common-bin, samba-common, smbclient, swat, samba-doc, cifs-utils, libpam-smbpass, libsmbclient, libsmbclient-dev, winbind, samba-dbg, self-service-password [amd64], ausias-self-service-password-settings [amd64], ausias-self-service-password-artwork, mypaint, mypaint-data-hires, italc2-master, italc2-client, libitalc2, vsftpd, alien, unetbootin, multisystem, yad, atom, pve-headers, sm-dev-depends, ausias-cc-update-settings, avidemux, avidemux, fentprojectes-keyring, ausias-nolimits-sources, ausias-printers-centro, ausias-printers-depinf, ausias-printers-fol, ausias-printers-secretaria, ausias-printers-orientacio, ausias-printers-tic, ausias-printers-borsa-treball, ausias-printers-biblioteca, ausias-printers-caporalia, ausias-pdf-printer, webcam-datastore, pve-kernel-4.15, pve-headers-4.15, pve-headers
Description: Soulmate supported packages
 Just a dummy package to reference additional packages supported in soulmate

Package: sm-dev-depends
Architecture: any
Recommends: build-essential
Description: Soulmate development required packages
 Just a dummy package to reference additional packages required for soulmate development

Package: soulmate-firmware
Architecture: all
Depends: firmware-linux-free, firmware-linux-nonfree, firmware-misc-nonfree, broadcom-bt-firmware, firmware-iwlwifi, firmware-realtek
Description: Soulmate recommended firmware packages
 This (meta)package depends on firmware packages required by common
 hardware and some specific system of our environment

Package: soulmate-desktop
Architecture: all
Depends: soulmate-minimal (= ${binary:Version}), soulmate-standard (= ${binary:Version}), soulmate-default-settings, sm-meta-debian-core (= ${binary:Version}), mint-meta-cinnamon, sm-meta-debian-cinnamon (= ${binary:Version}), sm-fonts (= ${binary:Version}), sm-extra-fonts (= ${binary:Version}), sm-office (= ${binary:Version}), sm-multimedia (= ${binary:Version}), sm-inet (= ${binary:Version}), sm-java (= ${binary:Version}), sm-misc (= ${binary:Version}), zenity, cups-pdf, pdftk, cups, cups-bsd, cups-client, synaptic, gdebi, system-config-printer
Recommends: file-roller, hplip, xsane, shutter, libgoo-canvas-perl, wine, winetricks, network-manager-openvpn, network-manager-openvpn-gnome, gparted, ophcrack, pm-utils, boot-repair, gnome-font-viewer, soulmate-firmware|soulmate-pve-firmware, sm-mate (= ${binary:Version}), yad
Description: Soulmate generic desktop system
 This (meta)package depends on all of the packages for a generic
 soulmate desktop system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: soulmate-desktop-mate
Architecture: all
Depends: soulmate-desktop (= ${binary:Version}), slick-greeter-change-session, sm-mate (= ${binary:Version})
Description: Soulmate lightweight Mate Desktop system
 This (meta)package forces Mate as default graphical session. The intended use is
 for low resource computers

Package: sm-inf-virt-pve
Architecture: any
Depends: soulmate-pve [amd64] | soulmate-pve-core [amd64] , aurex-virtualbox-5.2, vagrant, sm-zfs, virt-viewer
Description: Soulmate-inf virtualization packages
 Proxmox VE and VirtualBox required packages for soulmate-inf metapackage

Package: sm-inf-virt
Architecture: any
Depends: aurex-virtualbox-5.2, vagrant
Description: Soulmate-inf virtualization packages
 VirtualBox required packages for soulmate-inf metapackage

Package: sm-inf-dev
Architecture: all
Depends: build-essential, git, git-svn, subversion, bzr, gphpedit, netbeans-installer, default-mysql-client, atom
Recommends: android-sdk, eclipse-jee-installer, eclipse-adt-plugin, android-studio-installer, geany, geany-plugins, sublime-text, xmlcopyeditor, amaya, mysql-workbench, sm-nodejs
Description: Soulmate-inf development tools
 Development tools for soulmate-inf metapackage

Package: sm-inf-misc
Architecture: all
Depends: openssh-server, aurex-update-manager, easy-rsa, wireshark, ch-home
Recommends: freemind, packet-tracer-installer
Description: Soulmate-inf miscellaneous tools
 Miscellaneous packages for soulmate-inf metapackage

Package: sm-zfs
Architecture: all
Depends: zfs-dkms, zfsutils-linux
Pre-Depends: soulmate-base-files (>= 1:9.5+soulmate18.09.20), spl-dkms (>= 0.6.5.9)
Recommends: freemind, packet-tracer-installer
Description: Soulmate ZFS packages
 ZFS support for soulmate

Package: soulmate-inf
Architecture: all
Depends: sm-inf-virt (= ${binary:Version}), sm-inf-dev (= ${binary:Version}), sm-inf-misc (= ${binary:Version}), soulmate-desktop (= ${binary:Version})
Description: Soulmate for Ciclos Informatica
 This (meta)package depends on all of the packages required for
 using soulmate desktop in Ciclos Formativos Informatica

Package: soulmate-kvm
Architecture: any
Depends: soulmate-libvirt, virt-manager, spice-client-gtk, gir1.2-spice-client-gtk-3.0, virt-viewer, ssh-askpass-gnome, python-guestfs
Description: Soulmate kvm virtualization packages
 KVM virtualization required packages for soulmate

Package: sm-ausias-netstation
Architecture: all
Depends: unattended-upgrades, ausias-cc-default-settings
Description: Soulmate addons for Ausias March Intranet
 This seed provides addons to soulmate desktop environment
 for network workstations in Ausias March Intranet

Package: sm-ausias-printstation
Architecture: all
Depends: ausias-papercut-client, ausias-printers-centro
Description: Soulmate printing addons for Ausias March Intranet
 This seed provides addons to soulmate desktop environment
 to allow printing in Ausias March Intranet

Package: soulmate-smr
Architecture: all
Depends: soulmate-inf (= ${binary:Version})
Description: Soulmate smr extensions
 This (meta)package depends on the packages for the smr soulmate
 desktop system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed. 
 This (meta)package depends on all of the packages required for
 using soulmate desktop in Ciclos Formativos Informatica

Package: soulmate-asir
Architecture: all
Depends: soulmate-inf (= ${binary:Version}), sm-inf-virt-pve (= ${binary:Version})
Description: Soulmate asir extensions
 This (meta)package depends on the packages for the asir soulmate
 desktop system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed. 
 This (meta)package depends on all of the packages required for
 using soulmate desktop in Ciclos Formativos Informatica

Package: soulmate-daw
Architecture: all
Depends: soulmate-inf (= ${binary:Version})
Recommends: sm-cinnamon2d-set-default
Description: Soulmate daw extensions
 This (meta)package depends on the packages for the daw soulmate
 desktop system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed. 
 This (meta)package depends on all of the packages required for
 using soulmate desktop in Ciclos Formativos Informatica

Package: ausias-classroom-common
Architecture: any
Depends: sm-ausias-netstation (= ${binary:Version}), sm-ausias-printstation (= ${binary:Version}), soulmate-pve-kernel [amd64], x11vnc, ssvnc
Description: soulmate classroom common extensions
 This (meta)package depends on the packages for the classroom soulmate
 desktop system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: ausias-classroom-teacher
Architecture: any
Depends: ausias-classroom-common (= ${binary:Version}), nl-share-desktop, clusterssh
Description: soulmate classroom teacher extensions
 This (meta)package depends on the packages for the classroom teacher soulmate
 desktop system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: ausias-classroom-student
Architecture: any
Depends: ausias-classroom-common (= ${binary:Version})
Description: soulmate classroom student extensions
 This (meta)package depends on the packages for the classroom student soulmate
 desktop system
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: ausias-desktop
Architecture: any
Depends: soulmate-desktop (= ${binary:Version}), sm-ausias-netstation (= ${binary:Version}), sm-ausias-printstation (= ${binary:Version})
Description: The CIPFP Ausias March intranet desktop
 This (meta)package extends soulmate desktop to provide acces to the CIPFP
 Ausias March intranet services (auth, shares, printing).
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: ausias-desktop-mate
Architecture: any
Depends: ausias-desktop (= ${binary:Version}), soulmate-desktop-mate (= ${binary:Version})
Description: The CIPFP Ausias March intranet lightweight Mate desktop
 This (meta)package extends lightweight soulmate Mate desktop to provide acces
 to the CIPFP Ausias March intranet services (auth, shares, printing).
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed.

Package: soulmate-tg3-kernel
Architecture: any
Depends: linux-image-3.16.0-7-amd64 [amd64], linux-headers-3.16.0-7-amd64 [amd64], linux-image-3.16.0-7-686-pae [i386], linux-headers-3.16.0-7-686-pae [i386]
Description: Metapackage to install linux-image 3.16 (Debian 8) to avoid tg3 problems
 This package installs linux-image-3.16.0-7 and related files from Debian 8 and set that
 entry as default in grub in order to avoid problems related to tg3 module in 4.x kernels.

Package: soulmate-tg3-kernel-set-default
Architecture: any
Depends: soulmate-tg3-kernel
Conflicts: soulmate-pve-kernel-set-default
Description: Fixes soulmate-tg3-kernel as default kernel option in grub
 This package forces the soulmate tg3 selected kernel as default kernel
 option in grub bootloader to avoid problems related to tg3 module
